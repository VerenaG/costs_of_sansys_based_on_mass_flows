###Cost estimations for sanitation systems based on outputs of GRASP (mass flow module) (http://www.tinyurl.com/eawag-grasp)
###January 2019
###This script is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.
###It was written within the scope of a master thesis under the framework of the GRASP project.
###It aims at calculating the costs for a set of example sanitation systems (SanSys) to illustrate the concept developed within the thesis.
###The concept shows how the costs of sanitation systems can be generically calculated depending on the technologies involved
###and the corresponding inflows as calculated by the Mass flow module (see GRASP).
###
###Author: Verena Germann (contact: verena.germann@student.boku.ac.at)
###
##############prep######
b2e<-32.57 ## applied exchange rate: euro -> ethiopian birr ##finanzen.net 7.1.2019
unit.craftsman<-0.45 ##Euro/hour from CLARA SPT (Langergraber et al., 2015)
unit.unskilled.labour<-0.15 ##Euro/hour from CLARA SPT
gasoline.price<-0.52 ###Euro/l from CLARA SPT

###clear environment first !
######## Choose for which Kebele and SanSys costs shall be calculated
kebele<-'Mehal Ketema' ##'Woze', 'Mehal Ketema', 'Chamo'
SanSys<-'66617' ##'66194' (C, MK, W), '66617' (MK), '66431' (C, MK), '66356' (C)

sub_lab<-c("phosphor", "nitrogen", "water", "totalsolids")
sub_names<-c("P", "N", "H2O", "TS")

####kebele and SanSys specific data and inflows####
if(kebele=="Mehal Ketema"){
  area<-1.174 #[km²] calculated from GIS
  PE<-6634  #person equivalent
  pop.dens<-PE/area #[PE/km²] #population density
  length<-c(17318.72, 8702.622) ###[m] ##17318.72 (5 PE per HH) 8702.622 (25 PE per HH) estimated length of sewer system from Maurer 2009 see ##sewer length estimation
  ##if not available sewer length estimation:  74.761*(pop.dens*0.01)^(-0.8143)*PE ## regression model Maurer 2009 pop density [cap/ha] (x) by specific sewer length [m/cap]
  #the lower the number of PE per HH the better can it be approached by this function, 2PE per HH would lead to 32643.43 length
 
if(SanSys=="66431"){
  tech_names<-c("pour.flush", "conventional.sewer_1", "ABR_2_trans", "HSSFCW_3_trans", "cocomposting_6_trans", "application.of.compost_2", "irrigation_2", "irrigation_4")
  n_tech=length(tech_names)
  
  ##flow matrix [PE+substances x tech of the SanSys] ##inflow from Mass flow module (P=phosphor, N=nitrogen, H2O=water, TS=total solids)
  inflow<-matrix(nrow = 5, ncol = length(tech_names))
  dimnames(inflow) = list(c('PE',sub_names), tech_names)
  
  ##Inflow conventional_sewer_1 blackwater [kg/yr P, N, TS or m³/yr for H2O]
  inflow['P','conventional.sewer_1']<- 3012.7
  inflow['N','conventional.sewer_1']<- 19012.9
  inflow['H2O','conventional.sewer_1']<- 20097.7
  inflow['TS','conventional.sewer_1']<- 207757.0
  
  ##Flow conventional_sewer - blackwater - ABR [m³, kg]
  inflow['P','ABR_2_trans']<- 2711.43
  inflow['N','ABR_2_trans']<- 15210.3
  inflow['H2O','ABR_2_trans']<- 17485.0
  inflow['TS','ABR_2_trans']<- 186981
  
  ##Flow ABR - effluent - HSSFCW [m³, kg]
  inflow['P','HSSFCW_3_trans']<- 1816.66
  inflow['N','HSSFCW_3_trans']<- 10799.3
  inflow['H2O','HSSFCW_3_trans']<- 16610.7
  inflow['TS','HSSFCW_3_trans']<- 78532.1
  
  ##Flow ABR & HSSFCW - sludge - co-composting (3) [m³, kg]
  inflow['P','cocomposting_6_trans']<- 894.772+744.83
  inflow['N','cocomposting_6_trans']<- 4410.99+2807.83
  inflow['H2O','cocomposting_6_trans']<- 699.4+332.215
  inflow['TS','cocomposting_6_trans']<- 108449+25130.3
}else if(SanSys=="66194")  
{tech_names<-c("pour.flush", "single.pit_3", "cocomposting_5", "application.of.compost", "irrigation")
  n_tech=length(tech_names)
  
  ##flow matrix [PE+substances x tech of the SanSys]
  inflow<-matrix(nrow = 5, ncol = length(tech_names))
  dimnames(inflow) = list(c('PE',sub_names), tech_names)
  
  ##Inflow single pit_3 blackwater [m³]
  inflow['P',2]<- 3012.7
  inflow['N',2]<- 19012.9
  inflow['H2O',2]<- 20097.7
  inflow['TS',2]<- 207757.0
  
  ##Flow singlepit - sludge - co-composting (3) [m³, kg]
  inflow['P',3]<- 1928.13
  inflow['N',3]<- 3422.32
  inflow['H2O',3]<- 3014.66
  inflow['TS',3]<- 124654
}else if(SanSys=="66617")  
{ tech_names<-c("pour.flush", "ABR_1", "motorized.transport.dry_6", "cocomposting_6_trans", "application.of.compost_2", "irrigation_2", "irrigation_1")
  n_tech=length(tech_names)
  
  ##flow matrix [PE+substances x tech of the SanSys]
  inflow<-matrix(nrow = 5, ncol = length(tech_names))
  dimnames(inflow) = list(c('PE',sub_names), tech_names)
  
  ##Inflow ABR_1 blackwater [m³]
  inflow['P',2]<- 3012.7
  inflow['N',2]<- 19012.9
  inflow['H2O',2]<- 20097.7
  inflow['TS',2]<- 207757.0
  
  ##Flow ABR_1 - sludge - motorized.transport.dry_6 [m³, kg]
  inflow['P',3]<- 994.19
  inflow['N',3]<- 5513.74
  inflow['H2O',3]<- 803.908
  inflow['TS',3]<- 120499
  
  ##Flow motorized.transport.dry_6 - sludge - co-composting [m³, kg]
  inflow['P',4]<- 974.307
  inflow['N',4]<- 5293.19
  inflow['H2O',4]<- 779.791
  inflow['TS',4]<- 118089
}else{stop("unknown SanSys")}
}else if(kebele=="Chamo")
{area<-1.927 #[km²] calculated from GIS
PE<-10374  
pop.dens<-PE/area #[PE/km²]
length<-c(21165.31, 10247.17) ##21165.31 (5 PE per HH) 10247.17 (25 PE per HH) estimated length of sewer system from Maurer 2009 see ##sewer length estimation
if(SanSys=="66431"){
  tech_names<-c("pour.flush", "conventional.sewer_1", "ABR_2_trans", "HSSFCW_3_trans", "cocomposting_6_trans", "application.of.compost_2", "irrigation_2", "irrigation_4")
  n_tech=length(tech_names)
  
  ##flow matrix [PE+substances x tech of the SanSys]
  inflow<-matrix(nrow = 5, ncol = length(tech_names))
  dimnames(inflow) = list(c('PE',sub_names), tech_names)
  
  ##Inflow conventional_sewer_1 blackwater [m³]
  inflow['P',2]<- 4711
  inflow['N',2]<- 29732
  inflow['H2O',2]<- 31428
  inflow['TS',2]<- 324883
  
  ##Flow conventional_sewer - blackwater - ABR [m³, kg]
  inflow['P','ABR_2_trans']<- 4240.08
  inflow['N','ABR_2_trans']<- 23785.4
  inflow['H2O','ABR_2_trans']<- 27342.4
  inflow['TS','ABR_2_trans']<- 292394
  
  ##Flow ABR - effluent - HSSFCW [m³, kg]
  inflow['P','HSSFCW_3_trans']<- 2841
  inflow['N','HSSFCW_3_trans']<- 16888
  inflow['H2O','HSSFCW_3_trans']<- 25975
  inflow['TS','HSSFCW_3_trans']<- 122806
  
  ##Flow ABR & HSSFCW - sludge - co-composting (3) [m³, kg]
  inflow['P','cocomposting_6_trans']<- 1399+1165
  inflow['N','cocomposting_6_trans']<- 6898+4391
  inflow['H2O','cocomposting_6_trans']<- 1094+520
  inflow['TS','cocomposting_6_trans']<- 169589+39298
}else if(SanSys=="66194")  
{tech_names<-c("pour.flush", "single.pit_3", "cocomposting_5", "application.of.compost", "irrigation")
n_tech=length(tech_names)

##flow matrix [PE+substances x tech of the SanSys]
inflow<-matrix(nrow = 5, ncol = length(tech_names))
dimnames(inflow) = list(c('PE',sub_names), tech_names)

##Inflow single pit_3 blackwater [m³]
inflow['P',2]<- 4711
inflow['N',2]<- 29732
inflow['H2O',2]<- 31428
inflow['TS',2]<- 324883

##Flow singlepit - sludge - co-composting (3) [m³, kg]
inflow['P',3]<- 3015
inflow['N',3]<- 5352
inflow['H2O',3]<- 4714
inflow['TS',3]<- 194930

##Flow co-composting - compost - application of compost_1 [m³, kg]

inflow['P',4]<- 2985
inflow['N',4]<- 3372
inflow['H2O',4]<- 4243
inflow['TS',4]<- 118907

##Flow co-composting - effluent - irrigation_1 [m³, kg]

inflow['P', 5]<- 30
inflow['N',5]<- 214
inflow['H2O',5]<- 236
inflow['TS',5]<- 5848
}else if(SanSys=="66356")  
{ tech_names<-c("pour.flush", "septic.tank_1", "cocomposting_5", "application.of.compost_1", "irrigation_1")
n_tech=length(tech_names)

##flow matrix [PE+substances x tech of the SanSys]
inflow<-matrix(nrow = 5, ncol = length(tech_names))
dimnames(inflow) = list(c('PE',sub_names), tech_names)

##Inflow septic tank_3 blackwater [m³]
inflow['P',2]<- 4711
inflow['N',2]<- 29732
inflow['H2O',2]<- 31428
inflow['TS',2]<- 324883

##Flow septic tank_1 - sludge - co-composting (3) [m³, kg]
inflow['P',3]<- 1413
inflow['N',3]<- 5946
inflow['H2O',3]<- 1571
inflow['TS',3]<- 107211

##Flow co-composting - compost - application of compost_1 [m³, kg]

inflow['P',4]<- 1399
inflow['N',4]<- 3746
inflow['H2O',4]<- 1414
inflow['TS',4]<- 65399

##Flow co-composting - effluent - irrigation_1 [m³, kg]

inflow['P', 5]<- 14
inflow['N',5]<- 238
inflow['H2O',5]<- 79
inflow['TS',5]<- 3216
}else{stop("unknown SanSys")}
}else if(kebele=="Woze")
{area<-1.095 #[km²] calculated from GIS
PE<-15250  
pop.dens<-PE/area #[PE/km²]
length<-c(25361.31, 12123.68) ##estimated length of sewer system from Maurer 2009 see ##sewer length estimation
if (SanSys=="66194") 
{tech_names<-c("pour.flush", "single.pit_3", "cocomposting_5", "application.of.compost", "irrigation")
n_tech=length(tech_names)

##flow matrix [PE+substances x tech of the SanSys]
inflow<-matrix(nrow = 5, ncol = length(tech_names))
dimnames(inflow) = list(c('PE',sub_names), tech_names)

##Inflow single pit_3 blackwater [m³ or kg]
inflow['P',2]<- 6925.6
inflow['N',2]<- 43706.2
inflow['H2O',2]<- 46199.9
inflow['TS',2]<- 477584

##Flow singlepit - sludge - co-composting (3) [m³, kg]
inflow['P',3]<- 4432.38
inflow['N',3]<- 7867.12
inflow['H2O',3]<- 6929.99
inflow['TS',3]<- 286551
}else{stop("unknown SanSys")}
}else{stop("unknown kebele")}

##Inflow pour flush
inflow['PE',1]<-PE
##CA ... area within which sanitation facility needs to be accessible 
CA<-0.02 # in [km²] maximal area that can be covered by facility (def.: within the compound, not shared with other families, for water: basic access=not more than 1000m away (WSH0302), intermediate= not more than 100m away) --> within 0.02km² (diagonal of 100m)

cf_available<-c("pour.flush", "single.pit_3", "septic.tank_1", "cocomposting_5", "ABR_1", "conventional.sewer_1", "motorized.transport.dry_6","ABR_2_trans", "HSSFCW_3_trans", "cocomposting_6_trans") ##techs for which cost functions are defined so far


####construct matrix####
##costs
IIC<-matrix(nrow = 2, ncol = n_tech+3)
IIC<-data.frame(IIC)
dimnames(IIC) = list(c("min","max"), c("costs", "unit", tech_names, "SanSys"))
IIC[,1]<-c("IIC","IIC")
IIC[,2]<-c("Euro","Euro")

OMC<-matrix(nrow = 2, ncol = n_tech+3)
OMC<-data.frame(OMC)
dimnames(OMC) = list(c("min","max"), c("costs", "unit", tech_names, "SanSys"))
OMC[,1]<-c("OMC","OMC")
OMC[,2]<-c("Euro/year","Euro/year")

##cost function
IIcf<-vector(length=length(cf_available))
names(IIcf) = c(cf_available)

OMcf<-vector(length=length(cf_available))
names(OMcf) = c(cf_available)

##number of facilities matrix [min, max x techs for which cf available]
nfac<-matrix(nrow = 2, ncol = length(cf_available))
dimnames(nfac) = list(c('min','max'), cf_available)

##max/min volume input in [m3 or kg/yr] vol = max/min volume per facility; vol_per_year = max/min volume per facility per year
vol<-matrix(nrow = 2, ncol = length(cf_available))
dimnames(vol) = list(c('min','max'),cf_available)

vol_per_year<-matrix(nrow = 2, ncol=length(cf_available))
dimnames(vol_per_year) = list(c('min','max'),cf_available)

##inflow parameter for the cost functions
inf_para<-matrix(nrow = 2, ncol=length(cf_available))
dimnames(inf_para) = list(c('min','max'), cf_available)

####constriants#### 
## pour flush
##(e.g. min/max per facility) pour flush
PE_i_U_t<-5
PE_shared_U_t<-25
PE.per.household<-c(PE_i_U_t,PE_shared_U_t)

##min/max number of user interface facilities (here for pour flush)
nfac['max',1]<-inflow['PE',1]/PE_i_U_t
nfac['min',1]<-max(1, inflow['PE',1]/PE_shared_U_t)

##single pit
##min/max number of facilities single pit
vol[,'single.pit_3']<-c(1.8,18) ##CLARA cesspit, valid for cesspit volume 1.8 - 18 m³, emptying interval 45 days
vol_per_year[,'single.pit_3']<-vol[,'single.pit_3']*365/45 #available volume per year per single pit
##max_U_per_singlepit<-5 # meaning maximal five ? 
inf_para$single.pit_3<-inflow['H2O','single.pit_3']/365*45 ### m³ required within 45 days

##min/max number of single pit
nfac[1,'single.pit_3']<-min(nfac[1,tech_names[grep('single.pit_3', tech_names)-1]], max(1, inflow['H2O','single.pit_3']/max(vol_per_year[,'single.pit_3']), area/CA))
nfac[2,'single.pit_3']<-max(min(inflow['H2O','single.pit_3']/min(vol_per_year[,'single.pit_3']),nfac[2,tech_names[grep('single.pit_3', tech_names)-1]]), nfac[1,'single.pit_3'])

##septic tank
##min/max number of facilities septic tank, cost function valid for 5 to 2000 PE, input parameter PE with 80L/c/d per septic tank
vol[,'septic.tank_1']<-c(5*80*1000,2000*80*1000)
vol_per_year[,'septic.tank_1']<-vol[,'septic.tank_1']*365
max_U_per_septic.tank<-5 # meaning maximal five U per septic tank -> excluded 
inf_para$septic.tank_1<-inflow['H2O','septic.tank_1']/80*1000/365 ### m³ per day per in total inf_para$septic.tank_1/nfac[ ... ,'septic.tank_1'] = per facility

##min/max number of septic tank
nfac['min','septic.tank_1']<-min(nfac[1,tech_names[grep('septic.tank_1', tech_names)-1]], max(1, inflow['H2O','septic.tank_1']/max(vol_per_year[,'septic.tank_1']), area/CA))
nfac['max','septic.tank_1']<-max(min(inflow['H2O','septic.tank_1']/min(vol_per_year[,'septic.tank_1']), nfac[2,tech_names[grep('septic.tank_1', tech_names)-1]]), nfac[1,'septic.tank_1'])


##cocomposting_5
##constriants cocomposting: Total compostable waste volume per day: 1-500m³/d (CLARA), maximal all available compost per day (sludge_cocomposting/365)
sludge_cocomposting_5<-(inflow['H2O', 'cocomposting_5'])*1.25 ##available sludge per year [m³/yr]
inf_para$cocomposting_5<-sludge_cocomposting_5

vol[,'cocomposting_5']<-c(1,(min(sludge_cocomposting_5/365,500)))
vol_per_year[,'cocomposting_5']<-vol[,'cocomposting_5']*365

##min/max number of cocomposting_5

nfac[1,'cocomposting_5']<-min(nfac[1,tech_names[grep('cocomposting_5', tech_names)-1]], max(min(sludge_cocomposting_5/vol_per_year[,'cocomposting_5']), area/CA))
nfac[2,'cocomposting_5']<-max(nfac[1,'cocomposting_5'], min((sludge_cocomposting_5/vol_per_year[1,'cocomposting_5']), nfac[2,tech_names[grep('cocomposting_5', tech_names)-1]]))


##ABR_1 transported blackwater
##constriants: ### valid for 10-1000 PE per unit with 80L/PE/d --> inf_para inflow['H2O','ABR_1']*1000/80 -- equivalent of PE with 80/L/d #CLARA

vol[,'ABR_1']<-c(10*80/1000,(min(inflow['H2O','ABR_1']/365,1000*80/1000)))
vol_per_year[,'ABR_1']<-vol[,'ABR_1']*365
CA<-0.02 # in [km²] maximal area that can be covered by facility (def.: within the compound, not shared with other families, for water: basic access=not more than 1000m away (WSH0302), intermediate= not more than 100m away) --> within 0.02km² (diagonal of 100m)

##min/max number of ABR

nfac[1,'ABR_1']<-min(nfac[1,tech_names[grep('ABR_1', tech_names)-1]], max(1, (min(inflow['H2O','ABR_1']/vol_per_year[,'ABR_1'])), area/CA))
nfac[2,'ABR_1']<-max(min(inflow['H2O','ABR_1']/min(vol_per_year[,'ABR_1']), nfac[2,tech_names[grep('ABR_1', tech_names)-1]]), nfac[1,'ABR_1'])


##conventional_sewer_1
##constriants conventional_sewer_1: no constraints --> nmax & nmin =1
nfac[,'conventional.sewer_1']<-c(1,1) ###assumption: one sewer --> nfac=1

##ABR_2_trans transported blackwater
##constriants: ### valid for 10-1000 PE per unit with 80L/PE/d --> inf_para inflow['H2O','ABR_2_trans']*1000/80 -- equivalent of PE with 80/L/d #CLARA

inf_para$ABR_2_trans<-inflow['H2O','ABR_2_trans']/(80*365/1000)


vol[,'ABR_2_trans']<-c(10*80/1000,(min(inflow['H2O','ABR_2_trans']/365,1000*80/1000)))
vol_per_year[,'ABR_2_trans']<-vol[,'ABR_2_trans']*365

##min/max number of ABR

nfac[1,'ABR_2_trans']<-max(1, (min(inflow['H2O','ABR_2_trans']/vol_per_year[,'ABR_2_trans'])))
nfac[2,'ABR_2_trans']<-nfac[1,'ABR_2_trans']

##HSSFCW_3_trans transported effluent
##constriants HSSFCW:  

area.per.PE<-5 #estimated 5 m2 area HSSFCW needed per PE with 80L/d ## if changed also cost function changes
required.area.HSSFCW<-inflow['H2O','HSSFCW_3_trans']/80*area.per.PE  ###PE equivalent for PE with 80L/d wastewater production
inf_para$HSSFCW_3_trans<-required.area.HSSFCW

vol[,'HSSFCW_3_trans']<-c(4*area.per.PE,min(required.area.HSSFCW, 2000*area.per.PE))   ###valid for 4-2000 PE with 80L/d wastewater production per unit, maximal required area = required.area.HSSFCW

##min/max number of HSSFCW

nfac[1,'HSSFCW_3_trans']<-max(1, (min(required.area.HSSFCW/vol[,'HSSFCW_3_trans'])))
nfac[2,'HSSFCW_3_trans']<-nfac[1,'HSSFCW_3_trans']

##cocomposting_6_trans transported sludge
##constriants cocomposting: Total compostable waste volume per day: 1-500m³/d (CLARA), maximal all available compost per day (sludge_cocomposting/365)

sludge_cocomposting_6_trans<-(inflow['H2O', 'cocomposting_6_trans'])*1.25 ##available sludge per year [m³/yr]
inf_para$cocomposting_6_trans<-sludge_cocomposting_6_trans

vol[,'cocomposting_6_trans']<-c(1,(min(sludge_cocomposting_6_trans/365,500)))
vol_per_year[,'cocomposting_6_trans']<-vol[,'cocomposting_6_trans']*365

##min/max number of cocomposting

nfac[1,'cocomposting_6_trans']<-max(1, (min(sludge_cocomposting_6_trans/vol_per_year[,'cocomposting_6_trans'])))
nfac[2,'cocomposting_6_trans']<-nfac[1,'cocomposting_6_trans']

##############define cost functions######

##can be extended
#func <- function(x) x+4 ##to add new function

IIcf$pour.flush<-1102/b2e #Mastewal, 2008 in Asrat Mindawche
OMcf$pour.flush<-435/b2e #Mastewal, 2008 in Asrat Mindawche

IIcf$single.pit_3 <- function(inf_para) (-4.8639*inf_para^2 + 241.94*inf_para + 1287.2) #CLARA inf_para = m³ required within 45 days per facility based on calculations with 20L/PE/d
OMcf$single.pit_3 <- function(inf_para) inf_para*1.46+21.43 #CLARA

IIcf$septic.tank_1 <- function(inf_para) if (inf_para>20) { #CLARA total PE with 80L/c/d / number of septic tanks
  (inf_para^0.564*329.5)
}else {
  (inf_para^0.468*665.4)
}
OMcf$septic.tank_1 <- function(inf_para) (inf_para^0.415*9.4) #CLARA

IIcf$cocomposting_5<- function(inf_para) (inf_para*5/3/365)^0.799*18799.290 ##inf_para=inflow of sludge in [m³/d], TS=20%, price of bulking material assumed to be 0$ (CLARA D5.3.) #CLARA
OMcf$cocomposting_5<- function(inf_para) (inf_para*5/3/365)^0.766*817.041 #CLARA


IIcf$ABR_1<- function(inf_para) if(inf_para<400){  ### valid for 10-1000 PE per unit with 80L/PE/d --> inf_para inflow['H2O','ABR_2_trans']*1000/80/365 -- equivalent of PE with 80/L/d #CLARA
  inf_para^0.4844*821.9480
}else{
  inf_para^2*(-0.012)+inf_para*35.488+3009.788
}
OMcf$ABR_1<- function(inf_para) inf_para^0.4579*7.9850 #CLARA


IIcf$ABR_2_trans<- function(inf_para) if(inf_para<400){  ### valid for 10-1000 PE per unit with 80L/PE/d --> inf_para inflow['H2O','ABR_2_trans']*1000/80/365 -- equivalent of PE with 80/L/d #CLARA
  inf_para^0.4844*821.9480
}else{
  inf_para^2*(-0.012)+inf_para*35.488+3009.788
}
OMcf$ABR_2_trans<- function(inf_para) inf_para^0.4579*7.9850 #CLARA


IIcf$HSSFCW_3_trans<- function(inf_para) if(inf_para<=135){ ###inf_para=inflow['H2O','HSSFCW_3_trans']/80*area.per.PE, valid for 4PE to 2000PE #CLARA
  inf_para*61.672+1866.586
} else{
  inf_para*57.775+5571.575
}
OMcf$HSSFCW_3_trans<- function(inf_para) if(inf_para<=135){ #CLARA ###inf_para=inflow['H2O','HSSFCW_3_trans']/80*area.per.PE
  inf_para*1.047+30.178
} else{
  inf_para*1.026+33.586
}

IIcf$cocomposting_6_trans<- function(inf_para) (inf_para*5/3/365)^0.799*18799.290 ##inf_para=inflow of sludge in [m³/d], TS=20%, price of bulking material assumed to be 0$ (CLARA D5.3.) #CLARA
OMcf$cocomposting_6_trans<- function(inf_para) (inf_para*5/3/365)^0.766*817.041 #CLARA


######conventional sewer###### 

###valid for an inflow of up to 855000m³/yr
##sewer length estimation
##length estimated from UWIM Max Maurer 2009, changed parameters: combined (!!) sewer
##A=1.927*100^2, 				# size of the catchment area [m2]
##f1=2,                              # shape factor of the catchment area 
##f2=1.2,                            # shape factor of the housing plots
##E=10374,				                    # number of inhabitants 
##Anz.G=2074.8,                        # number of buildings
##Q.E.spez=8.3/(1000*24*3600),	        # specific sewage coefficient  per inhabitants [m3*(E*s)^-1] (from inflow calculation for pour.flush 8.3L/P/d)

specific_inflow<-inflow['H2O', 'conventional.sewer_1']/inflow['PE',1]/365 ## # specific sewage coefficient  per inhabitants [m3*(E*d)^-1] 
####for max PE.per.HH --> minimal costs####
PE.per.HH<-max(PE.per.household)
estimated.length<-min(length)
depth.sewer<-1.5
peak.factor<-1.5
HH<-inflow['PE',1]/PE.per.HH
length.per.HH<-estimated.length/HH
average.daily.peakflow.per.HH<-specific_inflow*1000/86400*peak.factor*PE.per.HH ##convert from m³/d/PE to l/s/HH
average.distance.manholes<-35
DN<-c(160,200,250,315)
q<-c(0.0000407172364553217, 0.0000636206819614401, 0.0000994073155647501, 0.000157819054190597) ## from CLARA based on PC-based simplified sewer design Duncan 2001
HH.served<-(q/average.daily.peakflow.per.HH*DN^(13/6))
PE.served<-HH.served*PE.per.HH
m.served.160<-if(inflow['PE',1]>PE.served[1]){
  length.per.HH*HH.served[1]
} else {
  estimated.length
}
m.served.200<-if (m.served.160>estimated.length) {
  0
} else if(inflow['PE',1]>PE.served[2])
{length.per.HH*HH.served[2]-m.served.160
} else {
  estimated.length-m.served.160
}
m.served.250<-if (m.served.160+m.served.200>estimated.length) {
  0
} else if(inflow['PE',1]>PE.served[3])
{length.per.HH*HH.served[3]-(m.served.160+m.served.200)
} else {
  estimated.length-(m.served.160+m.served.200)
}
m.served.315<-if (m.served.160+m.served.200+m.served.250>estimated.length) {
  0
} else if(inflow['PE',1]>PE.served[4])
{length.per.HH*HH.served[4]-(m.served.160+m.served.200+m.served.250)
} else {
  estimated.length-(m.served.160+m.served.200+m.served.250)
}
m.served<-c(m.served.160, m.served.200, m.served.250, m.served.315)  
sewer.costs.matrix<-data.frame(cbind(DN, q, HH.served, PE.served, m.served))
sewer.costs.matrix$number.of.manholes<-m.served/average.distance.manholes
sewer.costs.matrix$cost.per.manhole<-c(-4.8*depth.sewer^2+58.1*depth.sewer+94.3,-4.3*depth.sewer^2+56.5*depth.sewer+95.4,3.8*depth.sewer^2+11.3*depth.sewer+149.7,0.3*depth.sewer^2+27*depth.sewer+145.9)
sewer.costs.matrix$costs.per.sewer.m<-c(2.4*depth.sewer+3.1, 2.4*depth.sewer+3.9,3.0*depth.sewer+4.9,3.3*depth.sewer+6.9)
sewer.costs.matrix$manhole.diam<-c(0.8, 0.8, 0.8, 1)
sewer.costs.matrix$length.minus.diam.manholes<-sewer.costs.matrix$m.served-sewer.costs.matrix$number.of.manholes*sewer.costs.matrix$manhole.diam
sewer.costs.matrix$costs_manholes_min<-sewer.costs.matrix$cost.per.manhole*sewer.costs.matrix$number.of.manholes
sewer.costs.matrix$costs_sewer_min<-sewer.costs.matrix$length.minus.diam.manholes*sewer.costs.matrix$costs.per.sewer.m

####for min PE.per.HH --> maximal costs####
PE.per.HH<-min(PE.per.household)
estimated.length<-max(length)
HH<-inflow['PE',1]/PE.per.HH
length.per.HH<-estimated.length/HH
average.daily.peakflow.per.HH<-specific_inflow*1000/86400*peak.factor*PE.per.HH ##convert from m³/d/PE to l/s/HH
HH.served<-(q/average.daily.peakflow.per.HH*DN^(13/6))
PE.served<-HH.served*PE.per.HH
m.served.160<-if(inflow['PE',1]>PE.served[1]){
  length.per.HH*HH.served[1]
} else {
  estimated.length
}
m.served.200<-if (m.served.160>estimated.length) {
  0
} else if(inflow['PE',1]>PE.served[2])
{length.per.HH*HH.served[2]-m.served.160
} else {
  estimated.length-m.served.160
}
m.served.250<-if (m.served.160+m.served.200>estimated.length) {
  0
} else if(inflow['PE',1]>PE.served[3])
{length.per.HH*HH.served[3]-(m.served.160+m.served.200)
} else {
  estimated.length-(m.served.160+m.served.200)
}
m.served.315<-if (m.served.160+m.served.200+m.served.250>estimated.length) {
  0
} else if(inflow['PE',1]>PE.served[4])
{length.per.HH*HH.served[4]-(m.served.160+m.served.200+m.served.250)
} else {
  estimated.length-(m.served.160+m.served.200+m.served.250)
}
m.served<-c(m.served.160, m.served.200, m.served.250, m.served.315)  
sewer.costs.matrix<-data.frame(cbind(sewer.costs.matrix$costs_manholes_min,sewer.costs.matrix$costs_sewer_min, DN, q, HH.served, PE.served, m.served))
sewer.costs.matrix$number.of.manholes<-m.served/average.distance.manholes
sewer.costs.matrix$cost.per.manhole<-c(-4.8*depth.sewer^2+58.1*depth.sewer+94.3,-4.3*depth.sewer^2+56.5*depth.sewer+95.4,3.8*depth.sewer^2+11.3*depth.sewer+149.7,0.3*depth.sewer^2+27*depth.sewer+145.9)
sewer.costs.matrix$costs.per.sewer.m<-c(2.4*depth.sewer+3.1, 2.4*depth.sewer+3.9,3.0*depth.sewer+4.9,3.3*depth.sewer+6.9)
sewer.costs.matrix$manhole.diam<-c(0.8, 0.8, 0.8, 1)
sewer.costs.matrix$length.minus.diam.manholes<-sewer.costs.matrix$m.served-sewer.costs.matrix$number.of.manholes*sewer.costs.matrix$manhole.diam
sewer.costs.matrix$costs_manholes_max<-sewer.costs.matrix$cost.per.manhole*sewer.costs.matrix$number.of.manholes
sewer.costs.matrix$costs_sewer_max<-sewer.costs.matrix$length.minus.diam.manholes*sewer.costs.matrix$costs.per.sewer.m

sewer.costs<-c(sum(sum(sewer.costs.matrix[1])+sum(sewer.costs.matrix[2])),sum(sum(sewer.costs.matrix$costs_manholes_max)+sum(sewer.costs.matrix$costs_sewer_max)))
IIcf$conventional.sewer_1<-function(nfac) nfac*sewer.costs
OMcf$conventional.sewer_1<-function(nfac) nfac*0.02*IIC$conventional.sewer_1

######motorized.transport.dry_6######

collection.interval<-if(tech_names[grep('motorized.transport.dry_6', tech_names)-1]=='ABR_1'){  ### valid for 10-1000 PE per unit with 80L/PE/d --> inf_para inflow['H2O','ABR_2_trans']*1000/80/365 -- equivalent of PE with 80/L/d #CLARA
  365
}else{
  45
}

average.distance.to.pick.up<-c(20,10) ### from Meinzinger 2010 distance estimations for Arba Minch 15-/+ 5 km
time.spend.at.customer<-45 ###mins
average.speed<-20 ##km/h
vol.per.location.per.interval<-inflow['H2O','motorized.transport.dry_6']*1.25/nfac[,tech_names[grep('motorized.transport.dry_6', tech_names)-1]]/365*collection.interval ##inflow motorized per location of pick up, as generalization TS=20% of sludge assumed
vehilce.load<-5 ##m³
sludge.collected.per.trip<-min(vehilce.load,vol.per.location.per.interval)
number.of.trips.per.location<-ceiling(vol.per.location.per.interval/sludge.collected.per.trip)
hours.per.day<-10 ##working hours per day
working.days.per.year<-295
unit.price.vacuum.truck<-52195.27
gasoline.consumption<-25 ##l/100km

##nfac n-1 min
location.served.per.trip<-floor(max(1, vehilce.load/vol.per.location.per.interval[1]))
time.per.trip<-2*average.distance.to.pick.up[1]/average.speed+time.spend.at.customer/60### per 60 min = per hour
trips.per.day<-floor(max(hours.per.day/time.per.trip,1))
number.of.locations<-nfac[1,tech_names[grep('motorized.transport.dry_6', tech_names)-1]]
loc.per.day<-trips.per.day/number.of.trips.per.location[1]
max.loc.per.vehicle<-floor(loc.per.day*working.days.per.year)
number.of.vehicle<-ceiling(number.of.locations/max.loc.per.vehicle)
nfac[1,'motorized.transport.dry_6']<-number.of.vehicle

##OMcf for motorized.transport.dry_6

distance<-min(trips.per.day*working.days.per.year*average.distance.to.pick.up[1],2*average.distance.to.pick.up[1]*number.of.locations*365/collection.interval*number.of.trips.per.location[1])
annual.gasoline.costs<-gasoline.price*gasoline.consumption/100*distance
maintenance.costs<-0.1*number.of.vehicle*unit.price.vacuum.truck
labour.costs<-number.of.vehicle*hours.per.day*working.days.per.year*(unit.craftsman+unit.unskilled.labour)
OMcf$motorized.transport.dry_6[1]<-annual.gasoline.costs+maintenance.costs+labour.costs

##nfac n-1 max

location.served.per.trip<-floor(max(1, vehilce.load/vol.per.location.per.interval[2]))
time.per.trip<-2*average.distance.to.pick.up[2]/average.speed+time.spend.at.customer/60### per 60 min = per hour
trips.per.day<-floor(max(hours.per.day/time.per.trip,1))
number.of.locations<-nfac[2,tech_names[grep('motorized.transport.dry_6', tech_names)-1]]
loc.per.day<-trips.per.day/number.of.trips.per.location[2]
max.loc.per.vehicle<-floor(loc.per.day*working.days.per.year)
number.of.vehicle<-ceiling(number.of.locations/max.loc.per.vehicle)
nfac[2,'motorized.transport.dry_6']<-number.of.vehicle
IIcf$motorized.transport.dry_6<-function (nfac) nfac*unit.price.vacuum.truck

distance<-min(trips.per.day*working.days.per.year*average.distance.to.pick.up[2],2*average.distance.to.pick.up[2]*number.of.locations*365/collection.interval*number.of.trips.per.location)
annual.gasoline.costs<-gasoline.price*gasoline.consumption/100*distance
maintenance.costs<-0.1*number.of.vehicle*unit.price.vacuum.truck
labour.costs<-number.of.vehicle*hours.per.day*working.days.per.year*(unit.craftsman+unit.unskilled.labour)
OMcf$motorized.transport.dry_6[2]<-annual.gasoline.costs+maintenance.costs+labour.costs


##############calculate costs############# 
#####pour flush#####
##initial investment costs pour flush 

IIC[1,'pour.flush']<-nfac[[1,'pour.flush']]*IIcf[['pour.flush']]
IIC[2,'pour.flush']<-nfac[[2,'pour.flush']]*IIcf[['pour.flush']]

##operation&maintenance costs pour flush

OMC[1,'pour.flush']<-nfac[[1,'pour.flush']]*OMcf[['pour.flush']]
OMC[2,'pour.flush']<-nfac[[2,'pour.flush']]*OMcf[['pour.flush']]

#####single pit#####
 
##initial investment costs single pit

IIC[2,'single.pit_3']<-nfac[[2, 'single.pit_3']]*IIcf$single.pit_3(inf_para$single.pit_3/nfac[2,'single.pit_3'])

if (nfac[2,'single.pit_3']>nfac[1,'single.pit_3']) {
  IIC[1,'single.pit_3']<-nfac[1,'single.pit_3']*(IIcf$single.pit_3(inf_para$single.pit_3/nfac[1,'single.pit_3']))
} else {
  IIC[1,'single.pit_3']<-nfac[1,'single.pit_3']*(IIcf$single.pit_3(inf_para$single.pit_3/nfac[2,'single.pit_3']))
}
##operation&maintenance costs single pit

OMC[2,'single.pit_3']<-nfac[2,'single.pit_3']*OMcf$single.pit_3(inf_para$single.pit_3/nfac[2,'single.pit_3'])

if (nfac[2,'single.pit_3']>nfac[1,'single.pit_3']) {
  OMC[1,'single.pit_3']<-nfac[1,'single.pit_3']*(OMcf$single.pit_3(inf_para$single.pit_3/nfac[1,'single.pit_3']))
} else {
  OMC[1,'single.pit_3']<-nfac[1,'single.pit_3']*min(OMcf$single.pit_3(inf_para$single.pit_3/nfac[2,'single.pit_3']))
}

#####septic.tank_3#####

##initial investment costs septic.tank_3 

IIC[2,'septic.tank_1']<-nfac[[2, 'septic.tank_1']]*IIcf$septic.tank_1(inf_para$septic.tank_1/nfac[2,'septic.tank_1'])

if (nfac[2,'septic.tank_1']>nfac[1,'septic.tank_1']) {
  IIC[1,'septic.tank_1']<-nfac[1,'septic.tank_1']*(IIcf$septic.tank_1(inf_para$septic.tank_1/nfac[1,'septic.tank_1']))
} else {
  IIC[1,'septic.tank_1']<-nfac[1,'septic.tank_1']*(IIcf$septic.tank_1(inf_para$septic.tank_1/nfac[2,'septic.tank_1']))
}
##operation&maintenance costs septic.tank_3 (2)

OMC[2,'septic.tank_1']<-nfac[2,'septic.tank_1']*OMcf$septic.tank_1(inf_para$septic.tank_1/nfac[2,'septic.tank_1'])

if (nfac[2,'septic.tank_1']>nfac[1,'septic.tank_1']) {
  OMC[1,'septic.tank_1']<-nfac[1,'septic.tank_1']*(OMcf$septic.tank_1(inf_para$septic.tank_1/nfac[1,'septic.tank_1']))
} else {
  OMC[1,'septic.tank_1']<-nfac[1,'septic.tank_1']*min(OMcf$septic.tank_1(inf_para$septic.tank_1/nfac[2,'septic.tank_1']))
}

#####ABR_1#####
##initial investment costs ABR

inf_para$ABR_1<-inflow['H2O','ABR_1']/(80*365/1000)

IIC[2,'ABR_1']<-nfac[2,'ABR_1']*(IIcf$ABR_1(inf_para$ABR_1/nfac[2,'ABR_1']))

if (nfac[2,'ABR_1']>nfac[1,'ABR_1']) {
  IIC[1,'ABR_1']<-nfac[1,'ABR_1']*(IIcf$ABR_1(inf_para$ABR_1/nfac[1,'ABR_1']))
} else {
  IIC[1,'ABR_1']<-nfac[1,'ABR_1']*(IIcf$ABR_1(inf_para$ABR_1/nfac[2,'ABR_1']))
}

##operation and maintenance costs ABR

OMC[2,'ABR_1']<-nfac[2,'ABR_1']*OMcf$ABR_1(inf_para$ABR_1/nfac[2,'ABR_1'])

if (nfac[2,'ABR_1']>nfac[1,'ABR_1']) {
  OMC[1,'ABR_1']<-nfac[1,'ABR_1']*OMcf$ABR_1(inf_para$ABR_1/nfac[1,'ABR_1'])
} else {
  OMC[1,'ABR_1']<-nfac[1,'ABR_1']*OMcf$ABR_1(inf_para$ABR_1/nfac[2,'ABR_1'])
}


#####cocomposting_5#####

##initial investment costs cocomposting

IIC[2,'cocomposting_5']<-nfac[2,'cocomposting_5']*(IIcf$cocomposting_5(inf_para$cocomposting_5/nfac[2,'cocomposting_5']))
if (nfac[2,'cocomposting_5']>nfac[1,'cocomposting_5']) {
  IIC[1,'cocomposting_5']<-nfac[1,'cocomposting_5']*(IIcf$cocomposting_5(inf_para$cocomposting_5/nfac[1,'cocomposting_5']))
} else {
  IIC[1,'cocomposting_5']<-nfac[1,'cocomposting_5']*(IIcf$cocomposting_5(inf_para$cocomposting_5/nfac[2,'cocomposting_5']))
}

##operation and maintenance costs cocomposting
OMC[2,'cocomposting_5']<-nfac[2,'cocomposting_5']*(OMcf$cocomposting_5(inf_para$cocomposting_5/nfac[2,'cocomposting_5']))
if (nfac[2,'cocomposting_5']>nfac[1,'cocomposting_5']) {
  OMC[1,'cocomposting_5']<-nfac[1,'cocomposting_5']*(OMcf$cocomposting_5(inf_para$cocomposting_5/nfac[1,'cocomposting_5']))
} else {
  OMC[1,'cocomposting_5']<-nfac[1,'cocomposting_5']*min(OMcf$cocomposting_5(inf_para$cocomposting_5/nfac[2,'cocomposting_5']))
}

#####conventional sewer#####

IIC[1,'conventional.sewer_1']<-min(IIcf$conventional.sewer_1(nfac[1,'conventional.sewer_1']))
IIC[2,'conventional.sewer_1']<-max(IIcf$conventional.sewer_1(nfac[2,'conventional.sewer_1']))

OMC[1,'conventional.sewer_1']<-min(OMcf$conventional.sewer_1(nfac[1,'conventional.sewer_1']))
OMC[2,'conventional.sewer_1']<-max(OMcf$conventional.sewer_1(nfac[2,'conventional.sewer_1']))

#####motorized.transport.dry_6#####

IIC[1,'motorized.transport.dry_6']<-min(IIcf$motorized.transport.dry_6(nfac[,'motorized.transport.dry_6']))
IIC[2,'motorized.transport.dry_6']<-max(IIcf$motorized.transport.dry_6(nfac[,'motorized.transport.dry_6']))

OMC[1,'motorized.transport.dry_6']<-min(OMcf$motorized.transport.dry_6)
OMC[2,'motorized.transport.dry_6']<-max(OMcf$motorized.transport.dry_6)

#####ABR_2_trans#####
##initial investment costs ABR

IIC[2,'ABR_2_trans']<-nfac[2,'ABR_2_trans']*(IIcf$ABR_2_trans(inf_para$ABR_2_trans/nfac[2,'ABR_2_trans']))

if (nfac['max','ABR_2_trans']>nfac[1,'ABR_2_trans']) {
  IIC[1,'ABR_2_trans']<-nfac[1,'ABR_2_trans']*(IIcf$ABR_2_trans(inf_para$ABR_2_trans/nfac[1,'ABR_2_trans']))
} else {
  IIC[1,'ABR_2_trans']<-nfac[1,'ABR_2_trans']*(IIcf$ABR_2_trans(inf_para$ABR_2_trans/nfac[2,'ABR_2_trans']))
}

##operation and maintenance costs ABR

OMC[2,'ABR_2_trans']<-nfac[2,'ABR_2_trans']*(OMcf$ABR_2_trans(inf_para$ABR_2_trans/nfac[2,'ABR_2_trans']))

if (nfac[2,'ABR_2_trans']>nfac[1,'ABR_2_trans']) {
  OMC[1,'ABR_2_trans']<-nfac[1,'ABR_2_trans']*(OMcf$ABR_2_trans(inf_para$ABR_2_trans/nfac[1,'ABR_2_trans']))
} else {
  OMC[1,'ABR_2_trans']<-nfac[1,'ABR_2_trans']*(OMcf$ABR_2_trans(inf_para$ABR_2_trans/nfac[2,'ABR_2_trans']))
}


#####HSSFCW#####

##initial investment costs HSSFCW

IIC[2,'HSSFCW_3_trans']<-nfac[2,'HSSFCW_3_trans']*(IIcf$HSSFCW_3_trans(inf_para$HSSFCW_3_trans/nfac[2,'HSSFCW_3_trans']))
if (nfac[2,'HSSFCW_3_trans']>nfac[1,'HSSFCW_3_trans']) {
  IIC[1,'HSSFCW_3_trans']<-nfac[1,'HSSFCW_3_trans']*(IIcf$HSSFCW_3_trans(inf_para$HSSFCW_3_trans/nfac[1,'HSSFCW_3_trans']))
} else {
  IIC[1,'HSSFCW_3_trans']<-nfac[1,'HSSFCW_3_trans']*(IIcf$HSSFCW_3_trans(inf_para$HSSFCW_3_trans/nfac[2,'HSSFCW_3_trans']))
}

##operation and maintenance costs HSSFCW
OMC[2,'HSSFCW_3_trans']<-nfac[2,'HSSFCW_3_trans']*(OMcf$HSSFCW_3_trans(inf_para$HSSFCW_3_trans/nfac[2,'HSSFCW_3_trans']))

if (nfac[2,'HSSFCW_3_trans']>nfac[1,'HSSFCW_3_trans']) {
  OMC[1,'HSSFCW_3_trans']<-nfac[1,'HSSFCW_3_trans']*(OMcf$HSSFCW_3_trans(inf_para$HSSFCW_3_trans/nfac[1,'HSSFCW_3_trans']))
} else {
  OMC[1,'HSSFCW_3_trans']<-nfac['min','HSSFCW_3_trans']*min(OMcf$HSSFCW_3_trans(inf_para$HSSFCW_3_trans/nfac[2,'HSSFCW_3_trans']))
}

#####cocomposting_6_trans#####

##initial investment costs cocomposting

IIC[2,'cocomposting_6_trans']<-nfac[2,'cocomposting_6_trans']*(IIcf$cocomposting_6_trans(inf_para$cocomposting_6_trans/nfac[2,'cocomposting_6_trans']))
if (nfac['max','cocomposting_6_trans']>nfac[1,'cocomposting_6_trans']) {
  IIC[1,'cocomposting_6_trans']<-nfac[1,'cocomposting_6_trans']*(IIcf$cocomposting_6_trans(inf_para$cocomposting_6_trans/nfac[1,'cocomposting_6_trans']))
} else {
  IIC[1,'cocomposting_6_trans']<-nfac[1,'cocomposting_6_trans']*(IIcf$cocomposting_6_trans(inf_para$cocomposting_6_trans/nfac[2,'cocomposting_6_trans']))
}

##operation and maintenance costs cocomposting
OMC[2,'cocomposting_6_trans']<-nfac['max','cocomposting_6_trans']*(OMcf$cocomposting_6_trans(inf_para$cocomposting_6_trans/nfac[2,'cocomposting_6_trans']))
if (nfac['max','cocomposting_6_trans']>nfac['min','cocomposting_6_trans']) {
  OMC[1,'cocomposting_6_trans']<-nfac['min','cocomposting_6_trans']*(OMcf$cocomposting_6_trans(inf_para$cocomposting_6_trans/nfac[1,'cocomposting_6_trans']))
} else {
  OMC[1,'cocomposting_6_trans']<-nfac['min','cocomposting_6_trans']*min(OMcf$cocomposting_6_trans(inf_para$cocomposting_6_trans/nfac[2,'cocomposting_6_trans']))
}


#############sum costs of all techs#############

costs<-rbind(IIC[,c('costs','unit', tech_names)], OMC[,c('costs','unit', tech_names)])
costs$SanSys<-rowSums(costs[,1:n_tech+2], na.rm=TRUE) ##costs of the whole SanSys

assign(paste("costs",kebele, "SanSys", SanSys, sep="_"),costs)
costs
